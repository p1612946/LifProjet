# LIFProjet - AM1

You are in the ReadMe of the Python project that we carried out at the end of the subject
LIFProject of the L3 Computer SCience of the University of Lyon 1. Our subject was entitled
* "Recognizing characters or musical notes in an image and reading the text". *

** This program allows the sound playback of an image. **

#### * Python version: 3.6 *

## Installing dependencies
Our project uses OpenCV to detect text areas in the image and then
Keras and TensorFlow to classify the characters in the image.
Here is how to install them, as well as the other libraries that we need.

The following commands must be entered into a terminal.

As the dependencies are numerous, in order to avoid any ambiguity during the installation,
it is better to use the most specific commands possible.
For example instead of

`` '' pip install <lib> `` ''

rather privilege

`` '' python3 -m pip install --user <lib> `` ''



### OpenCV

The dependencies of OpenCV:
`` ``
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
`` ``
Once the dependencies are installed, create a folder that will be used for downloading and compiling OpenCV.
For example :
`` ``
mkdir ~ / opencv
cd ~ / opencv
`` ``
Now we need to clone the OpenCV git repository:
`` ``
git clone https://github.com/opencv/opencv.git

`` ``
Then, you have to create a temporary folder that will serve as a build folder for CMake:
`` ``
mkdir build
cd build
`` ``
We launch the configuration:
`` ``
cmake -D CMAKE_BUILD_TYPE = Release -D CMAKE_INSTALL_PREFIX = / usr / local ..
`` ``
We launch the Makefile, it is recommended to do it with several threads:
`` ``
make -j7 # we start 7 threads
`` ``
Finally, we launch the installation:
`` ``
sudo make install
`` ``

### TesserOCR
`` ``
python3 -m pip install --user tesserocr
`` ``

### Pillow
`` ``
python3 -m pip install --user Pillow
`` ``

### TensorFlow
`` ``
python3 -m pip install --user tensorflow
`` ``

### Keras
`` ``
sudo -H python3 -m pip install --user keras
`` ``

### gTTS
`` ``
python3 -m pip install --user gTTS

`` ``
### PyGObject
`` ``
sudo apt install python-gi python-gi-cairo python3-gi python3-gi-cairo gir1.2-gtk-3.0
`` ``
If that is not enough, maybe you should also do: `` python3 -m pip install --user PyGObject```

### Other necessary dependencies

##### Singing
`` ``
python3 -m pip install --user pyenchant
`` ``

##### Playsound
`` ``
python3 -m pip install --user playsound
`` ``



## Use

There are two ways to use the program. The first launches the interface with
aim to read the text of one image while the second starts training
of the convolutional neural network.
For the rest, we suppose to be in the main directory.

#### Reading

To launch the interface, just enter the command:

`` 'python3 ImgRead.py```

Be careful though because you need an internet connection for the program to work.

You can do tests with the images in the "IMGtests" folder.


#### Training

If you want to start the RNC training, you must first download
the archive with the directory containing the training and validation images
at this address: https://drive.google.com/file/d/1_EDAMNw81UDYJgVWG6F1k7WtY_r8vKPj/view

Once the archive has been downloaded, simply extract the folder in the main directory.
Finally, to start the training you must enter the following command:

`` 'python3 ImgRead.py --train```

This will do 10 training epochs on the database in the folder
"testDataBase".

At the end of this training, the new RNC weights will be stored in the file
"bottleneck_fc_model2.h5"

# LIFProjet - AM1

Vous êtes dans le ReadMe du projet Python que nous avons réalisé a l'issu de l'UE
LIFProjet de la L3 Informatique de l'Université Lyon 1. Notre sujet avait pour intitulé 
*"Reconnaissance de caractères ou de notes de musique sur une image et lecture du texte".*

**Ce programme permet la lecture sonore d'une image.**

#### *Version de Python : 3.6*

## Installation des dépendences
Notre projet utilise OpenCV afin de détecter les zones de texte de l'image puis 
Keras et TensorFlow pour faire la classification des caractères de l'image.
Voici comment les installer, ainsi que les autres librairies qui nous sont nécessaire.

Les commandes qui suivent doivent être entrées dans un terminal.

Comme les dépendences sont nombreuses, afin d'éviter toute ambiguité lors de l'installation,
il vaut mieux utiliser les commandes les plus spécifiques possibles.
Par exemple au lieu de

```pip install <lib>```

privilégier plutôt

```python3 -m pip install --user <lib>```



### OpenCV

Les dépendences d'OpenCV :
```
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
```
Une fois les dépendences intallées, créer un dossier qui va servir au téléchargement et la compilation d'OpenCV.
Par exemple :
```
mkdir ~/opencv
cd ~/opencv
```
Il faut maintenant cloner le dépôt git d'OpenCV :
```
git clone https://github.com/opencv/opencv.git

```
Ensuite, il faut créer un dossier temporaire qui va servir de dossier de build pour CMake :
```
mkdir build
cd build
```
On lance la configuration :
```
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
```
On lance le Makefile, il est recommandé de le faire avec plusieurs threads :
```
make -j7 # on lance 7 threads
```
Enfin, on lance l'installation :
```
sudo make install
```

### TesserOCR
```
python3 -m pip install --user tesserocr
```

### Pillow
```
python3 -m pip install --user Pillow
```

### TensorFlow
```
python3 -m pip install --user tensorflow
```

### Keras
```
sudo -H python3 -m pip install --user keras
```

### gTTS
```
python3 -m pip install --user gTTS

```
### PyGObject
```
sudo apt install python-gi python-gi-cairo python3-gi python3-gi-cairo gir1.2-gtk-3.0
```
Si cela ne suffit pas, il faut peut-être aussi faire : ```python3 -m pip install --user PyGObject```

### Autres dépendences nécessaires

##### Enchant
```
python3 -m pip install --user pyenchant
```

##### Playsound
```
python3 -m pip install --user playsound
```



## Utilisation

Il y a deux manières d'utiliser le programme. La première lance l'interface avec
pour but de lire le texte d'une image tandis que la deuxième lance l'entraînement
du réseau de neurones convolutif.
Pour la suite on suppose être dans le répertoire principal.

#### Lecture

Pour lancer l'interface il suffit d'entrer la commande :

```python3 ImgRead.py```

Attention cependant car il faut une connexion internet pour que le programme marche.

On peut faire des test avec les images dans le dossier "IMGtests".


#### Entraînement

Si l'on souhaite lancer l'entraînement du RNC, tout d'abord il faut télécharger
l'archive avec le répertoire contenant les images d'entrainement et de validation
à cette adresse : https://drive.google.com/file/d/1_EDAMNw81UDYJgVWG6F1k7WtY_r8vKPj/view

Une fois l'archive téléchargée il suffit d'extraire le dossier dans le répertoire principal.
Enfin, pour lancer l'entrainement il faut entrer la commande suivante :

```python3 ImgRead.py --train```

Ceci va faire 10 epochs d'entraînement sur la base de donnée se trouvant dans le dossier
"testDataBase".

A l'issue de cet entraînement, les nouveaux poids du RNC seront stockés dans le fichier
"bottleneck_fc_model2.h5"