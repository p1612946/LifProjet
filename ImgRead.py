from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from keras.preprocessing import image
import cv2
import tesserocr as tr
from PIL import Image
import os
import sys
import gtts
from gtts import gTTS
import enchant
from playsound import playsound
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

#fichier qui contient l'entrainement
top_model_weights_path = 'bottleneck_fc_model2.h5'

class ImgReadWin(Gtk.Window):
    imgPath = ""
    imgText = ""
    spellcheckUsage = False
    ttsLang = "en"

    def __init__(self):
        #Permet d'initialiser tous les éléments de l'interface.
        Gtk.Window.__init__(self, title="ImgRead")
        Gtk.Window.set_default_size(self, 350,400)
        Gtk.Window.set_position(self, Gtk.WindowPosition.CENTER)

#Box conteneur principal
        self.mainBox = Gtk.VBox(spacing=3)
        self.add(self.mainBox)

#Box charger et traiter fichier
        imgHandlerBox = Gtk.VBox()

        handlerBoxLabel = Gtk.Label("Selectionner une image à traîter :")

        loadImgBox = Gtk.HBox()
        self.displayImg = Gtk.Image()
        self.displayImg.props.height_request=200
        self.displayImg.props.width_request=300
        loadImgButton = Gtk.Button(label="Selectionner une image")
        loadImgButton.connect("clicked", self.file_chooser_clicked)
        loadImgBox.pack_start(self.displayImg, expand=False, fill=False, padding=0)
        loadImgBox.pack_start(loadImgButton, expand=False, fill=False, padding = 3)

        spellcheckButton = Gtk.CheckButton(label="Spellcheck (Anglais uniquement)")
        spellcheckButton.props.focus_on_click = False
        spellcheckButton.connect("toggled", self.spellcheck_button_toggled)
        handleButton = Gtk.Button(label="Traîter l'image")
        handleButton.connect("clicked", self.handle_button_clicked)

        imgHandlerBox.pack_start(handlerBoxLabel, expand=False, fill=False, padding = 3)
        imgHandlerBox.pack_start(loadImgBox, expand=False, fill=False, padding = 3)
        imgHandlerBox.pack_start(spellcheckButton, expand=False, fill=False, padding = 3)
        imgHandlerBox.pack_start(handleButton, expand=False, fill=False, padding = 10)


        self.mainBox.pack_start(imgHandlerBox, expand=False, fill=False, padding = 0)

#Box afficher texte
        textDisplayBox = Gtk.VBox(spacing=3)
        textDisplayLabel = Gtk.Label("Texte de l'image")
        self.textDisplayBuffer = Gtk.TextBuffer()
        textViewScroll = Gtk.ScrolledWindow()
        textViewScroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        textViewScroll.props.height_request=100
        textDisplayView = Gtk.TextView(buffer=self.textDisplayBuffer)
        textDisplayView.props.editable=False
        textDisplayView.props.overwrite=True
        textDisplayView.props.cursor_visible=False
        textDisplayView.props.wrap_mode=Gtk.WrapMode.WORD
        textViewScroll.add(textDisplayView)

        textDisplayBox.pack_start(textDisplayLabel, expand=False, fill=False, padding=3)
        textDisplayBox.pack_start(textViewScroll, expand=False, fill=False, padding=0)

        self.mainBox.pack_start(textDisplayBox, expand=False, fill=False, padding=0)

#Drop down list des langues
        langDDListBox = Gtk.VBox(spacing=3)

        langDDListLabel = Gtk.Label("Selectionner la langue de lecture :")

        langDict = gtts.lang.tts_langs()
        langDDList = Gtk.ComboBoxText()
        for id, text in langDict.items():
            langDDList.append(id, text)
        langDDList.set_active_id("en")

        langDDList.connect("changed", self.lang_selected)

        langDDListBox.pack_start(langDDListLabel, expand=False, fill=False, padding=0)
        langDDListBox.pack_start(langDDList, expand=False, fill=False, padding=0)

        self.mainBox.pack_start(langDDListBox, expand=False, fill=False, padding=0)

#Button play
        playButton = Gtk.Button(label="Play")
        playButton.connect("clicked", self.play_button_clicked)

        self.mainBox.pack_start(playButton, expand=False, fill=False, padding=0)

    def file_chooser_clicked(self, button):
        #Gestion du bouton de selection de fichier.
        dialog = Gtk.FileChooserDialog("Sélectionner un fichier", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_file_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.imgPath = dialog.get_filename()
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(dialog.get_filename())
            pixbuf = pixbuf.scale_simple(300, 200, GdkPixbuf.InterpType.BILINEAR)
            self.displayImg.set_from_pixbuf(pixbuf)

        elif response == Gtk.ResponseType.CANCEL:
            pass

        dialog.destroy()


    def add_file_filters(self, dialog):
        #Permet de n'afficher que les fichiers .jpg et .img quand on selectionne l'image.
        filter_img = Gtk.FileFilter()
        filter_img.set_name("Image files")
        filter_img.add_pattern("*.jpg")
        filter_img.add_pattern("*.png")
        dialog.add_filter(filter_img)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    def spellcheck_button_toggled(self, button):
        #Gestion du bouton de correcteur d'orthographe
        self.spellcheckUsage = button.get_active()

    def handle_button_clicked(self, button):
        #Gestion du bouton de traîtement
        l = self.opencv()
        classifier = self.initCNN()
        self.predictCNN(classifier,l)


        with open("textfile.txt", "r") as f:
            self.imgText = f.read()
            self.textDisplayBuffer.set_text(self.imgText)

        for dir in os.listdir("./croppedText/croppedLetters"):
            for file in os.listdir("./croppedText/croppedLetters/"+dir):
                os.remove("./croppedText/croppedLetters/"+dir+"/"+file)
            os.rmdir("./croppedText/croppedLetters/"+dir)

    def lang_selected(self, combo):
        #Gestion du bouton de sélection de langue de lecture
        self.ttsLang = combo.get_active_id()

    def play_button_clicked(self, button):
        #Gestion du bouton de lecture du texte
        if(self.imgText != ""):
            tts = gTTS(self.imgText, lang=self.ttsLang, slow=False)
            tts.save("tts.mp3")
            playsound("tts.mp3")

    def spellcheck(self, string):
        #Corrige string grâce à la lib enchant.
        newstring = ""
        d = enchant.Dict("en_US")
        for word in string.split(" "):
            if word != "":
                if d.check(word) is True:
                    newstring = newstring + word + " "
                else:
                    suggestList = d.suggest(word)
                    print("Les suggestions pour le mot \"" + word + "\" sont:")
                    print(suggestList)
                    newstring = newstring + suggestList[0] + " "

        return newstring


    def initCNN(self):
        #Initialise notre Réseau de neurones convolutif.
        classifier = Sequential()

        # Etape 1 - Convolution
        classifier.add(Conv2D(32, (3, 3), input_shape = (32,32,3), activation = 'relu'))

        # Etape 2 - Pooling
        classifier.add(MaxPooling2D(pool_size = (2, 2)))

        # Ajout d'une seconde couche de convolution.
        classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
        classifier.add(MaxPooling2D(pool_size = (2, 2)))

        # Etape 3 - Flattening
        classifier.add(Flatten())

        # Etape 4 - Connection totale
        classifier.add(Dense(units = 128, activation = 'relu'))
        classifier.add(Dense(units = 52, activation = 'softmax'))

        # Compilation du RNC.
        classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])
        return classifier

#Permet de classifier les parties de texte.
    def predictCNN(self, classifier, l):
        #la variable ou on va stocker la chaine des caracteres finale
        s = ""
        for i in range(len(l)):
        #si la qualite des lettres n'est pas bonnes et incomprehensible a lire, on a mis par default une chaine de caracteres vide
            if l[i] == ' ':
                s += ' '
            else:
                test_image = image.load_img(l[i], target_size = (32,32))
                test_image = image.img_to_array(test_image)
                test_image = np.expand_dims(test_image, axis = 0)
                classifier.load_weights(top_model_weights_path)
                result = classifier.predict(test_image)
                indice = np.argmax(result)
                if indice == 0:
                    s+= 'A'
                elif indice == 1:
                    s+= 'B'
                elif indice == 2:
                    s+= 'C'
                elif indice == 3:
                    s+= 'D'
                elif indice == 4:
                    s+= 'E'
                elif indice == 5:
                    s+= 'F'
                elif indice == 6:
                    s+= 'G'
                elif indice == 7:
                    s+= 'H'
                elif indice == 8:
                    s+= 'I'
                elif indice == 9:
                    s+= 'J'
                elif indice == 10:
                    s+= 'K'
                elif indice == 11:
                    s+= 'L'
                elif indice == 12:
                    s+= 'M'
                elif indice == 13:
                    s+= 'N'
                elif indice == 14:
                    s+= 'O'
                elif indice == 15:
                    s+= 'P'
                elif indice == 16:
                    s+= 'Q'
                elif indice == 17:
                    s+= 'R'
                elif indice == 18:
                    s+= 'S'
                elif indice == 19:
                    s+= 'T'
                elif indice == 20:
                    s+= 'U'
                elif indice == 21:
                    s+= 'V'
                elif indice == 22:
                    s+= 'W'
                elif indice == 23:
                    s+= 'X'
                elif indice == 24:
                    s+= 'Y'
                elif indice == 25:
                    s+= 'Z'
                elif indice == 26:
                    s+= 'a'
                elif indice == 27:
                    s+= 'd'
                elif indice == 28:
                    s+= 'c'
                elif indice == 29:
                    s+= 'b'
                elif indice == 30:
                    s+= 'e'
                elif indice == 31:
                    s+= 'f'
                elif indice == 32:
                    s+= 'g'
                elif indice == 33:
                    s+= 'h'
                elif indice == 34:
                    s+= 'i'
                elif indice == 35:
                    s+= 'j'
                elif indice == 36:
                    s+= 'k'
                elif indice == 37:
                    s+= 'l'
                elif indice == 38:
                    s+= 'm'
                elif indice == 39:
                    s+= 'n'
                elif indice == 40:
                    s+= 'o'
                elif indice == 41:
                    s+= 'p'
                elif indice == 42:
                    s+= 'q'
                elif indice == 43:
                    s+= 'r'
                elif indice == 44:
                    s+= 's'
                elif indice == 45:
                    s+= 't'
                elif indice == 46:
                    s+= 'u'
                elif indice == 47:
                    s+= 'v'
                elif indice == 48:
                    s+= 'w'
                elif indice == 49:
                    s+= 'x'
                elif indice == 50:
                    s+= 'y'
                elif indice == 51:
                    s+= 'z'
        #sauvegarde de la chaine de caracteres dans le fichier
        with open("textfile.txt", "w") as text_file:
            if self.spellcheckUsage == True:
                s=self.spellcheck(string=s)
            text_file.write(s)

# Scinde l'image en parties de texte puis en caractères unique.
    def opencv(self):
        ########################## PARTIE PRETRAITEMENT DE L'IMAGE##################################
        large = cv2.imread(self.imgPath)
        rgb = cv2.pyrDown(large)
        #convertis le tas d'image en greyscale
        small = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
        #preparation d'un kernel pour faire des operations morphologiques
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        grad = cv2.morphologyEx(small, cv2.MORPH_GRADIENT, kernel)
        #application d'un seuil pour obtenir une image blanc noir
        _, bw = cv2.threshold(grad, 127.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        #création l'élément de structure de dim 9x1 de la forme rectange pour les opérations morphologiques
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
        connected = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)
        #Contours - une courbe qui joint tous les points continus (a la limite) qui ont la meme couleur ou intensité.
        _ ,contours , hierarchy = cv2.findContours(connected.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        mask = np.zeros(bw.shape, dtype=np.uint8)
        #tableau qui va sauvegarder les positions de chaque mot lu
        indexMots = np.array([])
        #on lit les contours en descendant
        idx = len(contours)-1
        #on crée les dossiers où sont stockés les mots, si nécessaire.
        if not os.path.isdir("./croppedText/"):
            os.mkdir("./croppedText")
        if not os.path.isdir("./croppedText/croppedLetters/"):
            os.mkdir("./croppedText/croppedLetters")

        while (idx >= 0):
            x, y, w, h = cv2.boundingRect(contours[idx])
            mask[y:y+h, x:x+w] = 0
            cv2.drawContours(mask, contours, idx, (255, 255, 255), -1)
            r = float(cv2.countNonZero(mask[y:y+h, x:x+w])) / (w * h)
            if r > 0.45 and w > 8 and h > 8:
                height, width = rgb.shape[:2]
                #on coupe les rectangles de texte dans l'image originale
                #on teste les marges pour savoir ou on pourrait couper au maximum
                if (y-2>=0) and (x-2>=0) and (y+h+2<height) and (x+w+2<width):
                    crop_img = rgb[y-2:y+h+2, x-2:x+w+2]
                elif (y-1>=0) and (x-1>=0) and (y+h+2<height) and (x+w+2<width):
                    crop_img = rgb[y-1:y+h+2, x-1:x+w+2]
                elif (y-1>=0) and (x-1>=0) and (y+h+1<height) and (x+w+1<width):
                    crop_img = rgb[y-1:y+h+1, x-1:x+w+1]
                else:
                    crop_img = rgb[y:y+h, x:x+w]
                indexMots = np.append(indexMots,idx)
                #on cree un dossier pour chaque mot
                os.mkdir("./croppedText/croppedLetters/Mot"+str(idx))
                #on convert en blanc noir les images coupees
                crop_imgN = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
                _, bw = cv2.threshold(crop_imgN, 127.0, 255.0, cv2.THRESH_BINARY )
                s = "./croppedText/"+str(idx)+".jpg"
                cv2.imwrite(s,bw)
                #tracage des rectangles
                cv2.rectangle(rgb, (x, y), (x+w, y+h), (255, 0, 0), 2)
            idx -= 1
        ########################## PARTIE SEGMENTATION DES MOTS##################################
        indexMotsInt = indexMots.astype(int)
        #liste pour sauvegarder touts les chemins des mots
        l = []
        for i in range(indexMotsInt.size):
            dos = "./croppedText/croppedLetters/Mot" + str(indexMotsInt[i])
            img = cv2.imread("./croppedText/" + str(indexMotsInt[i]) + ".jpg")
            height, width = img.shape[:2]
            idxX = 0
            # comme l'API tesserocr prends des images PIL, on convertis l'image opencv en pil
            pil_img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
            # initialisation de l'api
            api = tr.PyTessBaseAPI()
            t = 0
            try:
                api.SetImage(pil_img)
                boxes = api.GetComponentImages(tr.RIL.SYMBOL, True)
                text = api.GetUTF8Text()
                for (im, box, _, _) in boxes:
                    x, y, w, h = box['x'], box['y'], box['w'], box['h']
                    crop_img = img[y:y+h, x:x+w]
                    s = dos + "/lettre"+str(t)+".jpg"
                    l.append(s)
                    crop_imgN = cv2.resize(crop_img, (32,32))
                    src = crop_imgN
                    height, width = img.shape[:2]
                    #on cree un contour blanc autour de la lettre
                    top = int(0.15*height)
                    bottom = int(0.15*height)
                    left = int(0.1*width)
                    right = int(0.1*width)
                    dst = cv2.copyMakeBorder(crop_imgN,top,bottom,left,right, cv2.BORDER_CONSTANT, value=[255,255,255] )
                    cv2.imwrite(s,dst)
                    t += 1
                idxX += 1
                l.append(' ')
            finally:
                api.End()
        return l

def trainCNN():
    """ Commence l'entaînement de notre RNC.

        Pour le lancer il faut lancer le programme avec l'argument
            --train
        Une fois l'entraînement terminé, elle sauvegarde les poids
        dans le fichier bottleneck_fc_model2.h5.
    """
    classifier = Sequential()

    classifier.add(Conv2D(32, (3, 3), input_shape = (32,32,3), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    classifier.add(Flatten())
    classifier.add(Dense(units = 128, activation = 'relu'))
    classifier.add(Dense(units = 52, activation = 'softmax'))

    classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])

    train_datagen = ImageDataGenerator(rescale = 1./255,
                                       shear_range = 0.2,
                                       zoom_range = 0.2,
                                       horizontal_flip = True)

    test_datagen = ImageDataGenerator(rescale = 1./255)

    training_set = train_datagen.flow_from_directory('testDataBase/Training',
                                                     target_size = (32, 32),
                                                     batch_size = 32,
                                                     class_mode = 'categorical')

    test_set = test_datagen.flow_from_directory('testDataBase/Testing',
                                                target_size = (32, 32),
                                                batch_size = 32,
                                                class_mode = 'categorical')

    classifier.fit_generator(training_set,
                             steps_per_epoch = 8000,
                             epochs = 10,
                             validation_data = test_set,
                             validation_steps = 2000)

    classifier.save_weights(top_model_weights_path)

if len(sys.argv) > 1:
    if sys.argv[1] == "--train":
        trainCNN()
else:
    window = ImgReadWin()
    window.connect("delete-event", Gtk.main_quit)
    window.show_all()
    Gtk.main()
